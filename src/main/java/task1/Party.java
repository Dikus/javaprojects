/*  We are having a party with amounts of tea and candy.
 Return the int outcome of the party encoded as 0=bad, 1=good, or 2=great.
 A party is good (1) if both tea and candy are at least 5.
 However, if either tea or candy is at least double the amount of the other one,
 the party is great (2). However, in all cases, if either tea or candy
is less than 5, the party is always bad (0). Read values of tea and candy from console. */
package task1;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;


public class Party {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static int party, tea, candy;

    public static void main(String[] args) {
        Party.inputAmounts();
        Party.partyStatus(tea, candy);
        Party.statusPrint();
    }

    static void inputAmounts() {
        try {
            System.out.println("Enter amount of tea ");
            tea = Integer.parseInt(br.readLine());
            System.out.println("Amount of tea = " + tea);

            System.out.println("Enter amount of candy");
            candy = Integer.parseInt(br.readLine());
            System.out.println("Amount of candy = " + candy);
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.out.println("Error");
        }
    }

    static int partyStatus(int tea, int candy) {

        if (tea >= 0 && candy >= 0) {
            party = ((candy < 5) || (tea < 5)) ? 0 : ((tea >= candy * 2) || (candy >= tea * 2)) ? 2 : 1;
        } else party = 3;
        return party;

    }

    static void statusPrint() {
        String result = party == 0 ? "bad" : party == 1 ? "good" : party == 3 ? "Did you steal some candies?" : "great";
        System.out.println(result);
    }
}
