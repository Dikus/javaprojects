/*  Given a string str, if the string starts with "f" return "Fizz".
If the string ends with "b" return "Buzz". If both the "f" and "b" conditions are true,
return "FizzBuzz". In all other cases, return the string unchanged. Read string from console.*/
package task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class FizzBuzz {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) {
        String input = inputString();
        System.out.println(stringProcessing(input));
    }

    static String inputString() {
        String input = "";
        System.out.println("Please enter a string: ");
        try {
            input = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    static String stringProcessing(String input) {
        String result = input;

        result = (((result.startsWith("f")) || (result.startsWith("F"))) && ((result.endsWith("b")) || (result.endsWith("B")))) ? "FizzBuzz" :
                (result.startsWith("f") || result.startsWith("F")) ? "Fizz" : (result.endsWith("b") || result.endsWith("B")) ? "Buzz" : result;

        return result;
    }
}