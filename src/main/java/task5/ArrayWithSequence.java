/* Given start and end numbers, return a new array containing the sequence of integers
from start up to but not including end, so start=5 and end=10 yields {5, 6, 7, 8, 9}.
The end number will be greater or equal to the start number.
Note that a length-0 array is valid. Read start and end from console.*/
package task5;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class ArrayWithSequence {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static int start, end;
    static int[] arr;

    public static void main(String[] args) {
        arrayInput();
        arrayPrint(arr);

    }

    static int[] arrayInput() {
        System.out.println("Enter start number:");
        try {
            start = Integer.parseInt(br.readLine());
            System.out.println("Enter end number:");
            end = Integer.parseInt(br.readLine());
            if (end > start) {
                arr = new int[end - start];
                for (int i = 0; i < arr.length; i++) {
                    arr[i] = start + i;
                }
            } else {
                System.out.println("Try with correct numbers");
                System.exit(0);
            }

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
        return arr;
    }

    static void arrayPrint(int[] arr) {
        System.out.println("Array: " + Arrays.toString(arr));
    }
}
