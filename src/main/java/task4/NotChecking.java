/*	Given a string, return a new string where "not " has been added to the front.
 However, if the string already begins with "not", return the string unchanged.
  Note: use .equals() to compare 2 strings. */
package task4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class NotChecking {
    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
    static String input;
    static String not = "not ";

    public static void main(String[] args) {
        inputStr();
        System.out.println(notChecking(input));
    }

    static void inputStr() {
        System.out.println("Enter a line");

        try {
            input = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    static String notChecking(String input) {
        input = (input.matches("^[Nn][Oo][Tt].*")) ? input : (not + input);
        return input;
    }
}
