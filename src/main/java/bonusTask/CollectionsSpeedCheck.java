/*
 Create a program which checks the speed of filling and sorting elements in different
 Collections implementations(where they aren't sorted). The result is displayed and written to a file.
*/
package bonusTask;

import java.io.*;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.*;
import java.util.stream.Collectors;


public class CollectionsSpeedCheck {

    private final static NumberFormat sec = new DecimalFormat();

    private final static File file = new File("C:\\Users\\adicusar\\Downloads\\Java tasks\\Task projects\\TasksPart1\\src\\main\\resources\\collectionSpeedResult.txt");
    private static BufferedWriter bw;
    private static Random rand = new Random();
    private static double startTime;
    private static double duration;
    private static BigDecimal durationBD;

    static {
        try {
            bw = new BufferedWriter(new FileWriter(file));
        } catch (IOException e) {
            System.out.println("Error in I/O");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        sec.setMaximumFractionDigits(25);

        ArrayList<Integer> arrayList = new ArrayList<>();

        LinkedList<Integer> linkedList = new LinkedList<>();

        HashSet<Integer> hashSet = new HashSet<>();

        TreeSet<Integer> treeSet = new TreeSet<>();

        HashMap<Integer, Integer> hashMap = new HashMap<>();

        TreeMap<Integer, Integer> treeMap = new TreeMap<>();

        System.out.println("Result for 100 elements added:\n");
        arrListCheck(100, arrayList);
        linkedListCheck(100, linkedList);
        hashSetCheck(100, hashSet);
        treeSetCheck(100, treeSet);
        hashMapCheck(100, hashMap);
        treeMapCheck(100, treeMap);
        bw.write("\n");

        System.out.println("\nResult for 1000 elements added:\n");
        arrListCheck(1000, arrayList);
        linkedListCheck(1000, linkedList);
        hashSetCheck(1000, hashSet);
        treeSetCheck(1000, treeSet);
        hashMapCheck(1000, hashMap);
        treeMapCheck(1000, treeMap);
        bw.write("\n");

        System.out.println("\nResult for 10000 elements added:\n");
        arrListCheck(10000, arrayList);
        linkedListCheck(10000, linkedList);
        hashSetCheck(10000, hashSet);
        treeSetCheck(10000, treeSet);
        hashMapCheck(10000, hashMap);
        treeMapCheck(10000, treeMap);
        bw.close();
    }

    private static void arrListCheck(int n, ArrayList<Integer> list) throws IOException {

        startTime = System.nanoTime();
        for (int i = 0; i < n; i++) {
            list.add(rand.nextInt());
        }
        Collections.sort(list);
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationBD = BigDecimal.valueOf(duration);
        System.out.println("Time elapsed for filling and sorting ArrayList in seconds = " + sec.format(durationBD));
        if (file.exists()) {
            bw.write("Time elapsed for filling and sorting " + n + " elements in ArrayList in seconds = " + durationBD + "\n");
        }
    }

    private static void linkedListCheck(int n, LinkedList<Integer> list) throws IOException {

        startTime = System.nanoTime();
        for (int i = 0; i < n; i++) {
            list.add(rand.nextInt());
        }
        Collections.sort(list);
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationBD = BigDecimal.valueOf(duration);
        System.out.println("Time elapsed for filling and sorting LinkedList in seconds = " + sec.format(durationBD));
        if (file.exists()) {
            bw.write("Time elapsed for filling and sorting " + n + " elements in LinkedList in seconds = " + durationBD + "\n");
        }
    }

    private static void hashSetCheck(int n, HashSet<Integer> set) throws IOException {

        startTime = System.nanoTime();
        for (int i = 0; i < n; i++) {
            set.add(rand.nextInt());
        }

        List<Integer> hsList = new ArrayList<>(set); // Using List interface on HashSet
        Collections.sort(hsList); // Sorting List obtained from HashSet
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationBD = BigDecimal.valueOf(duration);
        System.out.println("Time elapsed for filling and sorting HashSet in seconds = " + sec.format(durationBD));
        if (file.exists()) {
            bw.write("Time elapsed for filling and sorting " + n + " elements in HashSet in seconds = " + durationBD + "\n");
        }
    }

    private static void treeSetCheck(int n, TreeSet<Integer> set) throws IOException {

        startTime = System.nanoTime();
        for (int i = 0; i < n; i++) {
            set.add(rand.nextInt());
        }
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationBD = BigDecimal.valueOf(duration);
        System.out.println("Time elapsed for filling TreeSet in seconds = " + sec.format(durationBD));
        if (file.exists()) {
            bw.write("Time elapsed for filling " + n + " elements in TreeSet in seconds = " + durationBD + "\n");
        }
    }

    private static void hashMapCheck(int n, HashMap<Integer, Integer> map) throws IOException {

        startTime = System.nanoTime();
        for (int i = 0; i < n; i++) {
            map.put(rand.nextInt(), rand.nextInt());
        }
        /* Usually sorted hashMap is treeMap, but for example I have taken a sort example for Java 8,which has next steps:
         - Convert a Map into a Stream
         - Sort it
         - Collect and return a new LinkedHashMap (keep the order)
         */
        Map<Integer, Integer> sortedMap = map.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationBD = BigDecimal.valueOf(duration);
        System.out.println("Time elapsed for filling and sorting HashMap in seconds = " + sec.format(durationBD));
        if (file.exists()) {
            bw.write("Time elapsed for filling and sorting " + n + " elements in HashMap in seconds = " + durationBD + "\n");
        }
    }

    private static void treeMapCheck(int n, TreeMap<Integer, Integer> map) throws IOException {

        startTime = System.nanoTime();
        for (int i = 0; i < n; i++) {
            map.put(rand.nextInt(), rand.nextInt());
        }
        duration = (System.nanoTime() - startTime) / 1_000_000_000;
        durationBD = BigDecimal.valueOf(duration);
        System.out.println("Time elapsed for filling TreeMap in seconds = " + sec.format(durationBD));
        if (file.exists()) {
            bw.write("Time elapsed for filling " + n + " elements in TreeMap in seconds = " + durationBD + "\n");
        }
    }
}