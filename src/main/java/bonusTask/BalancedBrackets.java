/*
A bracket is considered to be any one of the following characters: (, ), {, }, [, or ].
Two brackets are considered to be a matched pair if the an opening bracket (i.e., (, [, or {) occurs to the left
of a closing bracket (i.e., ), ], or }) of the exact same type. There are three types of matched pairs of brackets: [], {}, and ().
A matching pair of brackets is not balanced if the set of brackets it encloses are not matched.
For example, {[(])}is not balanced because the contents in between { and } are not balanced.
The pair of square brackets encloses a single, unbalanced opening bracket, (, and the pair of parentheses
encloses a single, unbalanced closing square bracket, ].
By this logic, we say a sequence of brackets is considered to be balanced if the following conditions are met:
·  It contains no unmatched brackets.
·  The subset of brackets enclosed within the confines of a matched pair of brackets is also a matched pair of brackets.
Given  string of brackets, determine whether sequence of brackets is balanced.
If a string is balanced, print YES on a new line; otherwise, print NO on a new line.

 */
package bonusTask;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;


public class BalancedBrackets {
    static InputStreamReader input = new InputStreamReader(System.in);
    static BufferedReader reader = new BufferedReader(input);  // Entering data with buffered reader
    static String brackets;


    public static void main(String[] args) {
        String tmp = bracketsInput();
        System.out.println(bracketsCheck(tmp));
    }

    public static String bracketsInput() {
        try {
            System.out.println("Enter brackets: ");
            brackets = reader.readLine();
            input.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return brackets;
    }

    static String bracketsCheck(String brackets) {
        brackets = brackets.replaceAll("[^{}()\\]\\[]", ""); //Clearing string in case of non-brackets chars

        System.out.println("Brackets only string : " + brackets);
        if (brackets.isEmpty()) {
            System.out.println("String doesn't contain brackets");
            return "No";
        }
        char ch;
        Stack<Character> stack = new Stack<>();
        // Checking if there could be pairs of brackets in string
        if (brackets.length() % 2 != 0) return "No";
        else {
            for (int i = 0; i < brackets.length(); i++) {
                char bracketsChar = brackets.charAt(i);
                // Pushing open type brackets in stack to match after with close type
                if (bracketsChar == '[' || bracketsChar == '{' || bracketsChar == '(') {
                    stack.push(bracketsChar);
                    continue;
                }
                // Checking if there were only open type brackets in string
                if (bracketsChar == ']' || bracketsChar == '}' || bracketsChar == ')') {
                    if (!stack.empty()) {
                        ch = stack.pop(); // Popping element to check if it has needed pair
                        if (bracketsChar == ']' && ch == '[' || bracketsChar == '}' && ch == '{' || bracketsChar == ')' && ch == '(') {
                            continue;
                        } else {
                            return "No";
                        }
                    } else {
                        return "No";
                    }
                }
            }
        }
        if (stack.empty()) return "Yes";
        else return "No";
    }
}