/*	Given a day of the week encoded as 0=Sun, 1=Mon, 2=Tue, ...6=Sat,
and a boolean indicating if we are on vacation, return a string of the form "7:00"
indicating when the alarm clock should ring. Weekdays, the alarm should be "7:00" and
on the weekend it should be "10:00". Unless we are on vacation - then on weekdays
it should be "10:00" and weekends it should be "off".*/
package task3;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Alarm {
    static Calendar cal = Calendar.getInstance(); // Calendar
    static DateFormat parser = new SimpleDateFormat("dd/MM/yyyy"); //format of date
    static Scanner in = new Scanner(System.in);
    static String alarm;
    static boolean vacation;
    static int dayOfWeek;

    public static void main(String[] args)  {
        isVacation();
        setDate();
        System.out.println(alarmPrint(dayOfWeek, vacation));
    }

    static boolean isVacation() {
        vacation = false;

        System.out.println("Are you on vacation(y/n)");
        char v = in.findInLine(".").charAt(0);

        vacation = (v == 'y') ? true : (v == 'n') ? false : vacation;
        return vacation;
    }

    static int setDate() {
        System.out.println("Set date please: dd/mm/year");
        String dateStr = in.next(); // taking input for date to be parsed
        try {
            Date date = parser.parse(dateStr);
            cal.setTime(date); // setting date from parsed string

            System.out.println(date);
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        dayOfWeek = cal.get(Calendar.DAY_OF_WEEK); // getting number of day of the week
        return dayOfWeek;
    }

    static String alarmPrint(int dayOfWeek, boolean vacation) {
        alarm = ((dayOfWeek == 1 || dayOfWeek == 7) && vacation) ? "Alarm Clock: Off"
                : ((dayOfWeek > 1 && dayOfWeek < 7) && !vacation) ? "Alarm Clock Set On 7:00" : "Alarm Clock Set on 10:00";

        return alarm;
    }
}