package task4;

import org.junit.Test;

import static org.junit.Assert.*;
import static task4.NotChecking.notChecking;

public class NotCheckingTest {
    @Test
    public void testTwoWords() {
        assertEquals("not hello world", notChecking("hello world"));
    }

    @Test
    public void testOneWord() {
        assertEquals("not hello", notChecking("hello"));
    }

    @Test
    public void testStringWithNot() {
        assertEquals("not me", notChecking("not me"));
    }

    @Test
    public void testLongStringWithNot() {
        assertEquals("not really interested", notChecking("not really interested"));
    }

    @Test
    public void testUpperCaseStringWithNot() {
        assertEquals("NOT MINE", notChecking("NOT MINE"));
    }


}