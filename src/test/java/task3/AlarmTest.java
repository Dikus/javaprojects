package task3;

import org.junit.Test;

import static org.junit.Assert.*;
import static task3.Alarm.alarmPrint;

public class AlarmTest {
    @Test
    public void testWeekendVacation() {
        assertEquals("Alarm Clock: Off", alarmPrint(1, true));
    }

    @Test
    public void testWeekendNoVacation() {
        assertEquals("Alarm Clock Set on 10:00", alarmPrint(1, false));
    }

    @Test
    public void testWeekdayVacation() {
        assertEquals("Alarm Clock Set on 10:00", alarmPrint(2, true));
    }

    @Test
    public void testWeekdayNoVacation() {
        assertEquals("Alarm Clock Set On 7:00", alarmPrint(2, false));
    }


}