package task2;

import org.junit.Test;

import static org.junit.Assert.*;
import static task2.FizzBuzz.stringProcessing;

public class FizzBuzzTest {
    @Test
    public void testSmallFStartSmallBEnd() {
        assertEquals("FizzBuzz", stringProcessing("fab"));
    }

    @Test
    public void testSmallFStartBigBend() {
        assertEquals("FizzBuzz", stringProcessing("faB"));

    }

    @Test
    public void testBigFStartBigBEnd() {
        assertEquals("FizzBuzz", stringProcessing("FAB"));
    }

    @Test
    public void testBigFStartSmallBEnd() {
        assertEquals("FizzBuzz", stringProcessing("Fab"));
    }

    @Test
    public void testBigFStartNoBEnd() {
        assertEquals("Fizz", stringProcessing("Fan"));
    }

    @Test
    public void testSmallFStartNoBEnd() {
        assertEquals("Fizz", stringProcessing("fan"));
    }

    @Test
    public void testNoFStartBigBEnd() {
        assertEquals("Buzz", stringProcessing("caB"));
    }

    @Test
    public void testNoFStartSmallBend() {
        assertEquals("Buzz", stringProcessing("cab"));
    }


}