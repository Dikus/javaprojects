package bonusTask;

import org.junit.Test;

import static bonusTask.BalancedBrackets.bracketsCheck;
import static org.junit.Assert.*;

public class BalancedBracketsTest {

    @Test
    public void testNumericInput() {
        assertEquals("No", bracketsCheck("12345678"));
    }

    @Test
    public void testSpecialSymbolsInput() {
        // Testing special symbols without brackets
        assertEquals("No", bracketsCheck("*&%$^%"));
    }

    @Test
    public void testEmptyInput() {
        assertEquals("No", bracketsCheck(""));
    }

    @Test
    public void testAlphanumericInput() {
        assertEquals("No", bracketsCheck("123456hello"));
    }

    @Test
    public void testBracketsInput() {
        assertEquals("No", bracketsCheck("{[(])}"));
    }

    @Test
    public void testBalancedBracketsInput() {
        assertEquals("Yes", bracketsCheck("{()[]{}}"));
    }

    @Test
    public void testMixedInput() {
        assertEquals("No", bracketsCheck("hello${[(])}%#$"));
    }

    @Test
    public void testGoodMixedInput()
    {
        assertEquals("Yes",bracketsCheck("@$#%#{()[]{}}$hello"));
    }
}