package task1;

import org.junit.Test;

import static org.junit.Assert.*;
import static task1.Party.partyStatus;

public class PartyTest {
    // 0 states for "bad",1 for "good",2 for great, 3 for "Did you steal some candies?"

    @Test
    public void testSmallTea() {
        assertEquals(0, partyStatus(4, 6));
    }

    @Test
    public void testSmallCandy() {
        assertEquals(0, partyStatus(5, 4));
    }

    @Test
    public void testDoubleTeaSmallCandy() {
        assertEquals(0, partyStatus(6, 3));
    }

    @Test
    public void testDoubleCandySmallTea() {
        assertEquals(0, partyStatus(3, 6));
    }

    @Test
    public void testNegativeCandy() {
        assertEquals(3, partyStatus(3, -2));
    }

    @Test
    public void testNegativeTea() {
        assertEquals(3, partyStatus(-3, 4));
    }

    @Test
    public void testDoubleTeaManyCandy() {
        assertEquals(2, partyStatus(50, 25));
    }

    @Test
    public void testDoubleCandyMuchTea() {
        assertEquals(2, partyStatus(25, 50));
    }

    @Test
    public void testManyCandyMuchTea() {
        assertEquals(1, partyStatus(25, 25));
    }

}